#pragma once

#include <map>
#include "virtual\Geometry.h"
#include "shader\shader.h"

class Global {
private:
	static Global* instance;
	Global();
public:
	static Global* getInstance();
	std::map<std::string, Geometry> geometries;
	Shader* shader;
};