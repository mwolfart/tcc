#pragma once

#include <iostream>
#include <cstdio>

#include <imgui.h>
#include "window\ErrorWindow.h"
#include "window\RootWindow.h"
#include "window\TextureWindow.h"

#define ROOT_WINDOW				0
#define TEXTURE_WINDOW			1

class WindowManager {
private:
	bool showErrorWindow;
	bool showTextureWindow;
public:
	void renderWindows();
	void showWindow(int windowId);
	WindowManager();
};