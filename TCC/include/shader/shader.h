#pragma once

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <stdexcept>
#include <GL\glew.h>
#include <stb_image.h>

class Shader {
	typedef struct uniforms {
		GLint model;
		GLint view;
		GLint projection;
		GLint objectId;
		GLint bboxMin;
		GLint bboxMax;
	} Uniforms;

public:
	GLuint programId;
	GLuint vertexShaderId;
	GLuint fragmentShaderId;
	GLuint numLoadedTextures;
	Uniforms uniforms;

	void LoadShadersFromFiles();
	void LoadShaderVertex(const char* filename);
	void LoadShaderFragment(const char* filename);
	void LoadShader(const char* filename, GLuint shader_id);
	void CreateGpuProgram();
	void LoadTextures();
	void LoadTextureImage(const char* filename);
};