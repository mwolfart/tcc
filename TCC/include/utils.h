#pragma once

#include <glm\vec4.hpp>

float norm(glm::vec4 v);
glm::vec4 cross4(glm::vec4 u, glm::vec4 v);