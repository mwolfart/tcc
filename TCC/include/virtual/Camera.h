#pragma once

#define PI 3.14159265359

#include <glm\vec4.hpp>
#include <glm\mat4x4.hpp>

#include "virtual\SceneObject.h"
#include "utils.h"

//#include "matrices.h"

class Camera : public SceneObject {
public:
	Camera();

	glm::mat4	view;
	glm::mat4	projection;
private:
	glm::vec4	lookat = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
	glm::vec4	viewVec = lookat - position;
	glm::vec4	upVec = glm::vec4(0.0f, 1.0f, 0.0f, 0.0f);

	double nearplane = -0.1f;
	double farplane = -10.0f;
	double fov = PI / 3.0f;
	double screenRatio = 1.0f;

	void updateCameraViewMatrix();
	void updateCameraProjectionMatrix();
};