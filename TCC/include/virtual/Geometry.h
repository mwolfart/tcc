#pragma once

#include <string>
#include <GL\glew.h>
#include <glm\vec3.hpp>

class Geometry {
public:
	std::string	name;
	void*		firstIndex;				// �ndice do primeiro v�rtice dentro do vetor indices[] definido em BuildTrianglesAndAddToVirtualScene()
	int			numIndexes;				// N�mero de �ndices do objeto dentro do vetor indices[] definido em BuildTrianglesAndAddToVirtualScene()
	GLenum		renderingMode;			// Modo de rasteriza��o (GL_TRIANGLES, GL_TRIANGLE_STRIP, etc.)
	GLuint		vertexArrayObjectId;	// ID do VAO onde est�o armazenados os atributos do modelo
	glm::vec3	bboxMin;
	glm::vec3	bboxMax;
};