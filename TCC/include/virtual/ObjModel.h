#pragma once

#include <vector>
#include <algorithm>
#include <GL\glew.h>
#include <glm\vec4.hpp>

#include <tiny_obj_loader.h>

#include "virtual\Geometry.h"
#include "utils.h"

class ObjModel {
public:
	tinyobj::attrib_t					attrib;
	std::vector<tinyobj::shape_t>		shapes;
	std::vector<tinyobj::material_t>	materials;

	void ComputeNormals();
	Geometry BuildGeometry();
	ObjModel(const char* filename, const char* basepath = NULL, bool triangulate = true);
};