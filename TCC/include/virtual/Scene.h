#pragma once

#include <string>
#include <map>

#include "Global.h"

#include "virtual\Camera.h"
#include "virtual\SceneObject.h"
#include <glm\gtc\type_ptr.hpp>
#include <glm\vec3.hpp>

class Scene {
public:
	Camera camera = Camera();
	std::map<std::string, SceneObject> objects;

	void add(std::string name, SceneObject object);
	void remove(std::string name);
	void render();
	void drawVirtualObject(SceneObject object);
};