#pragma once

#include <cstdio>
#include <cstdlib>
#include <glm\vec4.hpp>
#include <glm\mat4x4.hpp>
#include "virtual\Geometry.h"

class SceneObject {
public:
	GLint		id;
	glm::vec4	position;
	glm::vec4	rotation;
	glm::vec4	scale;
	std::string	geometry;
	glm::mat4	model;
	
	void updateModel();
};