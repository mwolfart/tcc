#pragma once

#include <iostream>
#include <string>
#include <cstdio>

#include <imgui.h>
#include "Window.h"

class ErrorWindow : public Window {
public:
	static void render(WindowManager* wm, bool* showWindow, char* text);
};
