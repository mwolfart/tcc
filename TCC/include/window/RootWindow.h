#pragma once

#include <iostream>
#include <cstdio>

#include <imgui.h>
#include "Window.h"

class RootWindow : public Window {
public:
	static void render(WindowManager* wm);
};
