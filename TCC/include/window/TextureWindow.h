#pragma once

#include <iostream>
#include <cstdio>
#include <imgui.h>

#include "Global.h"

#include "Window.h"
#include "virtual\Scene.h"

class TextureWindow : public Window {
public:
	Scene scene = Scene();

	static void render(WindowManager* wm, bool* showWindow);
};
