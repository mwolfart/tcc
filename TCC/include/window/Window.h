#pragma once

#include <iostream>
#include <cstdio>

#include <imgui.h>

class WindowManager;

class Window {
public:
	virtual void render(WindowManager* wm);
};
