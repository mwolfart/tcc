#include "Global.h"

Global* Global::instance = 0;

Global::Global() {}

Global* Global::getInstance() {
	if (instance == 0) {
		instance = new Global();
	}

	return instance;
}