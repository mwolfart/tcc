#include "WindowManager.h"

WindowManager::WindowManager(void) {
	showErrorWindow = false;
	showTextureWindow = false;
}

void WindowManager::renderWindows() {
	RootWindow::render(this);

	if (showTextureWindow) TextureWindow::render(this, &showTextureWindow);
	if (showErrorWindow) ErrorWindow::render(this, &showErrorWindow, "The requested window does not exist.");
}

void WindowManager::showWindow(int windowId) {
	switch (windowId) {
	case TEXTURE_WINDOW:
		showTextureWindow = true;
		break;
	default:
		showErrorWindow = true;
	}
}