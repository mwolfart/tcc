#include "shader\shader.h"

void Shader::LoadShadersFromFiles() {
	LoadShaderVertex("../../TCC/src/shader/shader_vertex.glsl");
	LoadShaderFragment("../../TCC/src/shader/shader_fragment.glsl");

	// Deletamos o programa de GPU anterior, caso ele exista.
	if (programId != 0)
		glDeleteProgram(programId);

	// Criamos um programa de GPU utilizando os shaders carregados acima.
	CreateGpuProgram();

	// Buscamos o endere�o das vari�veis definidas dentro do Vertex Shader.
	// Utilizaremos estas vari�veis para enviar dados para a placa de v�deo
	// (GPU)! Veja arquivo "shader_vertex.glsl" e "shader_fragment.glsl".
	uniforms.model = glGetUniformLocation(programId, "model"); // Vari�vel da matriz "model"
	uniforms.view = glGetUniformLocation(programId, "view"); // Vari�vel da matriz "view" em shader_vertex.glsl
	uniforms.projection = glGetUniformLocation(programId, "projection"); // Vari�vel da matriz "projection" em shader_vertex.glsl
	uniforms.objectId = glGetUniformLocation(programId, "object_id"); // Vari�vel "object_id" em shader_fragment.glsl
	uniforms.bboxMin = glGetUniformLocation(programId, "bbox_min");
	uniforms.bboxMax = glGetUniformLocation(programId, "bbox_max");

	// Vari�veis em "shader_fragment.glsl" para acesso das imagens de textura
	glUseProgram(programId);
	glUniform1i(glGetUniformLocation(programId, "TextureImage0"), 0);
	glUniform1i(glGetUniformLocation(programId, "TextureImage1"), 1);
	//glUniform1i(glGetUniformLocation(programId, "TextureImage2"), 2);
	glUseProgram(0);
}

void Shader::LoadShaderVertex(const char* filename) {
	// Criamos um identificador (ID) para este shader, informando que o mesmo
	// ser� aplicado nos v�rtices.
	vertexShaderId = glCreateShader(GL_VERTEX_SHADER);

	// Carregamos e compilamos o shader
	LoadShader(filename, vertexShaderId);
}

void Shader::LoadShaderFragment(const char* filename) {
	// Criamos um identificador (ID) para este shader, informando que o mesmo
	// ser� aplicado nos fragmentos.
	fragmentShaderId = glCreateShader(GL_FRAGMENT_SHADER);

	// Carregamos e compilamos o shader
	LoadShader(filename, fragmentShaderId);
}

void Shader::LoadShader(const char* filename, GLuint shader_id) {
	// Lemos o arquivo de texto indicado pela vari�vel "filename"
	// e colocamos seu conte�do em mem�ria, apontado pela vari�vel
	// "shader_string".
	std::ifstream file;

	try {
		file.open(filename);
		file.fail() ? throw std::exception() : NULL;
	}
	catch (const std::exception& e) {
		(void)e; // Ignore warning
		std::cerr << "ERROR: Cannot open file \"" << filename << "\".\n";
	}

	std::stringstream shader;
	shader << file.rdbuf();
	std::string str = shader.str();
	const GLchar* shader_string = str.c_str();
	const GLint   shader_string_length = static_cast<GLint>(str.length());

	// Define o c�digo do shader GLSL, contido na string "shader_string"
	glShaderSource(shader_id, 1, &shader_string, &shader_string_length);

	// Compila o c�digo do shader GLSL (em tempo de execu��o)
	glCompileShader(shader_id);

	// Verificamos se ocorreu algum erro ou "warning" durante a compila��o
	GLint compiled_ok;
	glGetShaderiv(shader_id, GL_COMPILE_STATUS, &compiled_ok);

	GLint log_length = 0;
	glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &log_length);

	// Alocamos mem�ria para guardar o log de compila��o.
	// A chamada "new" em C++ � equivalente ao "malloc()" do C.
	GLchar* log = new GLchar[log_length];
	glGetShaderInfoLog(shader_id, log_length, &log_length, log);

	// Imprime no terminal qualquer erro ou "warning" de compila��o
	if (log_length != 0) {
		std::string status = compiled_ok ? "WARNING: " : "ERROR: ";
		std::cerr	<< status << "OpenGL compilation of \"" << filename << "\".\n"
					<< "== Start of compilation log\n" 
					<< log 
					<< "== End of compilation log\n";
	}

	// A chamada "delete" em C++ � equivalente ao "free()" do C
	delete[] log;
}

void Shader::CreateGpuProgram() {
	// Criamos um identificador (ID) para este programa de GPU
	programId = glCreateProgram();

	// Defini��o dos dois shaders GLSL que devem ser executados pelo programa
	glAttachShader(programId, vertexShaderId);
	glAttachShader(programId, fragmentShaderId);

	// Linkagem dos shaders acima ao programa
	glLinkProgram(programId);

	// Verificamos se ocorreu algum erro durante a linkagem
	GLint linkedOk = GL_FALSE;
	glGetProgramiv(programId, GL_LINK_STATUS, &linkedOk);

	// Imprime no terminal qualquer erro de linkagem
	if (linkedOk == GL_FALSE) {
		GLint logLength = 0;
		glGetProgramiv(programId, GL_INFO_LOG_LENGTH, &logLength);

		// Alocamos mem�ria para guardar o log de compila��o.
		// A chamada "new" em C++ � equivalente ao "malloc()" do C.
		GLchar* log = new GLchar[logLength];

		glGetProgramInfoLog(programId, logLength, &logLength, log);

		std::cerr	<< "ERROR: OpenGL linking of program failed.\n" 
					<< "== Start of link log\n"
					<< log
					<< "\n== End of link log\n";

		// A chamada "delete" em C++ � equivalente ao "free()" do C
		delete[] log;
	}

	// Os "Shader Objects" podem ser marcados para dele��o ap�s serem linkados
	glDeleteShader(vertexShaderId);
	glDeleteShader(fragmentShaderId);
}

void Shader::LoadTextures() {
	// Carregamos duas imagens para serem utilizadas como textura
	LoadTextureImage("../../TCC/data/tc-earth_daymap_surface.jpg");      // TextureImage0
	LoadTextureImage("../../TCC/data/tc-earth_nightmap_citylights.gif"); // TextureImage1
}

void Shader::LoadTextureImage(const char* filename) {
	std::cerr << "Loading image \"" << filename << "\"...";

	// Primeiro fazemos a leitura da imagem do disco
	stbi_set_flip_vertically_on_load(true);
	int width;
	int height;
	int channels;
	unsigned char *data = stbi_load(filename, &width, &height, &channels, 3);

	if (data == NULL) {
		std::cerr << "ERROR: Cannot open image file \"" << filename << "\"...";
	}

	std::cerr << "OK (" << width << "x" << height << ").\n";

	// Agora criamos objetos na GPU com OpenGL para armazenar a textura
	GLuint texture_id;
	GLuint sampler_id;
	glGenTextures(1, &texture_id);
	glGenSamplers(1, &sampler_id);

	// Veja slide 160 do documento "Aula_20_e_21_Mapeamento_de_Texturas.pdf"
	glSamplerParameteri(sampler_id, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glSamplerParameteri(sampler_id, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	// Par�metros de amostragem da textura. Falaremos sobre eles em uma pr�xima aula.
	glSamplerParameteri(sampler_id, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glSamplerParameteri(sampler_id, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	// Agora enviamos a imagem lida do disco para a GPU
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
	glPixelStorei(GL_UNPACK_SKIP_PIXELS, 0);
	glPixelStorei(GL_UNPACK_SKIP_ROWS, 0);

	GLuint textureunit = numLoadedTextures;
	glActiveTexture(GL_TEXTURE0 + textureunit);
	glBindTexture(GL_TEXTURE_2D, texture_id);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_SRGB8, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
	glGenerateMipmap(GL_TEXTURE_2D);
	glBindSampler(textureunit, sampler_id);

	stbi_image_free(data);

	numLoadedTextures += 1;
}