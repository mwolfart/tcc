#include "virtual\Camera.h"

Camera::Camera() {
	position = glm::vec4(-1.0f, -1.0f, -1.0f, 0.0f);
	viewVec = lookat - position;
	updateCameraViewMatrix();
	updateCameraProjectionMatrix();
}

void Camera::updateCameraViewMatrix() {
	glm::vec4 w = -viewVec;
	glm::vec4 u = cross4(upVec, w);

	// Normalizamos os vetores u e w
	w = w / norm(w);
	u = u / norm(u);

	glm::vec4 v = cross4(w, u);

	glm::vec4 origin_o = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);

	float ux = u.x;
	float uy = u.y;
	float uz = u.z;
	float vx = v.x;
	float vy = v.y;
	float vz = v.z;
	float wx = w.x;
	float wy = w.y;
	float wz = w.z;

	view = glm::mat4(
		ux, uy, uz, -glm::dot(u, position - origin_o),
		vx, vy, vz, -glm::dot(v, position - origin_o),
		wx, wy, wz, -glm::dot(w, position - origin_o),
		0.0f, 0.0f, 0.0f, 1.0f
	);
}

void Camera::updateCameraProjectionMatrix() {
	float f = farplane;
	float n = nearplane;

	float t = fabs(n) * tanf(fov / 2.0f);
	float b = -t;
	float r = t * screenRatio;
	float l = -r;

	glm::mat4 P = glm::mat4(
		n, 0.0f, 0.0f, 0.0f,
		0.0f, n, 0.0f, 0.0f,
		0.0f, 0.0f, n + f, -f*n,
		0.0f, 0.0f, 1.0f, 0.0f
	);

	glm::mat4 M = glm::mat4(
		2.0f / (r - l), 0.0f, 0.0f, -(r + l) / (r - l),
		0.0f, 2.0f / (t - b), 0.0f, -(t + b) / (t - b),
		0.0f, 0.0f, 2.0f / (f - n), -(f + n) / (f - n),
		0.0f, 0.0f, 0.0f, 1.0f
	);

	projection = -M*P;
}