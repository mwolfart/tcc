#include "virtual\Scene.h"

void Scene::add(std::string name, SceneObject object) {
	objects[name] = object;
}

void Scene::remove(std::string name) {
	objects.erase(name);
}

void Scene::render() {
	for (auto& obj : objects) {
		drawVirtualObject(obj.second);
	}
}

void Scene::drawVirtualObject(SceneObject object) {
	Global* global = Global::getInstance();

	glUniformMatrix4fv(global->shader->uniforms.model, 1, GL_FALSE, glm::value_ptr(object.model));
	glUniform1i(global->shader->uniforms.objectId, object.id);

	// "Ligamos" o VAO.
	glBindVertexArray(global->geometries[object.geometry].vertexArrayObjectId);

	glm::vec3 bbox_min = global->geometries[object.geometry].bboxMin;
	glm::vec3 bbox_max = global->geometries[object.geometry].bboxMax;
	glUniform4f(global->shader->uniforms.bboxMin, bbox_min.x, bbox_min.y, bbox_min.z, 1.0f);
	glUniform4f(global->shader->uniforms.bboxMax, bbox_max.x, bbox_max.y, bbox_max.z, 1.0f);

	// Pedimos para a GPU rasterizar os v�rtices dos eixos XYZ
	// apontados pelo VAO como linhas.
	glDrawElements(
		global->geometries[object.geometry].renderingMode,
		global->geometries[object.geometry].numIndexes,
		GL_UNSIGNED_INT,
		(void*)global->geometries[object.geometry].firstIndex
	);

	// "Desligamos" o VAO
	glBindVertexArray(0);
}