#include "virtual\SceneObject.h"

void SceneObject::updateModel() {
	float _cos;
	float _sin;

	glm::mat4 resizing = glm::mat4(
		scale.x, 0.0f, 0.0f, 0.0f,
		0.0f, scale.y, 0.0f, 0.0f,
		0.0f, 0.0f, scale.z, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	);

	_cos = cos(rotation.x);
	_sin = sin(rotation.x);
	glm::mat4 rotation_X = glm::mat4(
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, _cos, -_sin, 0.0f,
		0.0f, _sin, _cos, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	);

	_cos = cos(rotation.y);
	_sin = sin(rotation.y);
	glm::mat4 rotation_Y = glm::mat4(
		_cos, 0.0f, _sin, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		-_sin, 0.0f, _cos, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	);

	_cos = cos(rotation.z);
	_sin = sin(rotation.z);
	glm::mat4 rotation_Z = glm::mat4(
		_cos, -_sin, 0.0f, 0.0f,
		_sin, _cos, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	);

	glm::mat4 translation = glm::mat4(
		1.0f, 0.0f, 0.0f, position.x,
		0.0f, 1.0f, 0.0f, position.y,
		0.0f, 0.0f, 1.0f, position.z,
		0.0f, 0.0f, 0.0f, 1.0f
	);

	model = translation * rotation_Z * rotation_Y * rotation_X * resizing;
}