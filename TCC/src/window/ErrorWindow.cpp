#include "window\ErrorWindow.h"

void ErrorWindow::render(WindowManager* wm, bool* showWindow, char* text) {
	ImGui::Begin("Error", showWindow);
	ImGui::Text(text);
	if (ImGui::Button("Close"))
		*showWindow = false;
	ImGui::End();
}
