#include "window\RootWindow.h"
#include "WindowManager.h"

void RootWindow::render(WindowManager* wm) {
	ImGui::Begin("Root");

	if (ImGui::Button("Texture sample")) {
		wm->showWindow(TEXTURE_WINDOW);
	}

	ImGui::End();
}
